﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessagePanel : MonoBehaviour {

    // Use this for initialization
    public static MessagePanel Instent;
    RectTransform Okay;
    RectTransform Close;
    Text MessageText;

	void Start ()
    {
        Instent = this;
        Okay = transform.GetChild(0).GetChild(0).Find("OkayButton").GetComponent<RectTransform>();
        Close = transform.GetChild(0).GetChild(0).Find("CloseButton").GetComponent<RectTransform>();
        MessageText = transform.GetChild(0).GetChild(0).Find("MessageText").GetComponent<Text>();

    }

    public void Show(string Message, string OkayText, string CloseText, System.Action OkayButtonAction, System.Action CloseButtonAction)
    {
        transform.GetChild(0).gameObject.SetActive(true);
        MessageText.text = Message;
        Okay.gameObject.SetActive(true);
        Close.gameObject.SetActive(true);
        Okay.anchorMax = new Vector2(0, 0);
        Okay.anchorMin = new Vector2(0, 0);
        Okay.pivot = new Vector2(0, 0);
        Okay.anchoredPosition = new Vector2(20, 20);
        Close.anchorMin = new Vector2(1, 0);
        Close.anchorMax = new Vector2(1, 0);
        Close.pivot = new Vector2(1, 0);
        Okay.GetComponent<Button>().onClick.RemoveAllListeners();
        Okay.GetComponent<Button>().onClick.AddListener(() => OkayButtonAction());
        Okay.GetComponent<Button>().onClick.AddListener(() => ClosePanel());
        Okay.transform.GetChild(0).GetComponent<Text>().text = OkayText;
        Close.GetComponent<Button>().onClick.RemoveAllListeners();
        Close.GetComponent<Button>().onClick.AddListener(() => CloseButtonAction());
        Okay.GetComponent<Button>().onClick.AddListener(() => ClosePanel());
        Close.transform.GetChild(0).GetComponent<Text>().text = CloseText;
    }
    public void Show(string Message, string OkayText, string CloseText, System.Action OkayButtonAction)
    {
        transform.GetChild(0).gameObject.SetActive(true);
        MessageText.text = Message;
        Okay.gameObject.SetActive(true);
        Close.gameObject.SetActive(true);
        Okay.anchorMax = new Vector2(0, 0);
        Okay.anchorMin = new Vector2(0, 0);
        Okay.pivot = new Vector2(0, 0);
        Okay.anchoredPosition = new Vector2(20, 20);
        Close.anchorMin = new Vector2(1, 0);
        Close.anchorMax = new Vector2(1, 0);
        Close.pivot = new Vector2(1, 0);
        Okay.GetComponent<Button>().onClick.RemoveAllListeners();
        Okay.GetComponent<Button>().onClick.AddListener(() => OkayButtonAction());
        Okay.GetComponent<Button>().onClick.AddListener(() => ClosePanel());
        Okay.transform.GetChild(0).GetComponent<Text>().text = OkayText;
        Close.GetComponent<Button>().onClick.RemoveAllListeners();
        Close.GetComponent<Button>().onClick.AddListener(() => ClosePanel());
        Close.transform.GetChild(0).GetComponent<Text>().text = CloseText;
    }
    public void Show(string Message, string OkayText, System.Action OkayButtonAction)
    {
        transform.GetChild(0).gameObject.SetActive(true);
        MessageText.text = Message;
        Okay.gameObject.SetActive(true);
        Close.gameObject.SetActive(false);
        Okay.anchorMax = new Vector2(0.5f, 0);
        Okay.anchorMin = new Vector2(0.5f, 0);
        Okay.pivot = new Vector2(0.5f, 0);
        Okay.anchoredPosition = new Vector2(0, 20);
        Okay.GetComponent<Button>().onClick.RemoveAllListeners();
        Okay.GetComponent<Button>().onClick.AddListener(() => OkayButtonAction());
        Okay.GetComponent<Button>().onClick.AddListener(() => ClosePanel());
        Okay.transform.GetChild(0).GetComponent<Text>().text = OkayText;
        
    }
    public void Show(string Message, string OkayText)
    {
        transform.GetChild(0).gameObject.SetActive(true);
        MessageText.text = Message;
        Okay.gameObject.SetActive(true);
        Close.gameObject.SetActive(false);
        Okay.anchorMax = new Vector2(0.5f, 0);
        Okay.anchorMin = new Vector2(0.5f, 0);
        Okay.pivot = new Vector2(0.5f, 0);
        Okay.anchoredPosition = new Vector2(0, 20);
        Okay.GetComponent<Button>().onClick.RemoveAllListeners();
        Okay.GetComponent<Button>().onClick.AddListener(() => ClosePanel());
        Okay.transform.GetChild(0).GetComponent<Text>().text = OkayText;
    }
    public void ClosePanel()
    {
        transform.GetChild(0).gameObject.SetActive(false);
    }
}
