﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Star : MonoBehaviour {

    // Use this for initialization
    public static Star Instant { get; set; } 
    int TotalStar = 0;
    public Text StarTex;
    public GameObject OverPanel;
    private float minutes;
    private float seconds;
    public bool stop=false;
    public float timeLeft = 300.0f;
    public Text TimerText;
    string ScrenName;
    int TimeMode;
    void Start () {
        ScrenName = UnityEngine.SceneManagement.SceneManager.GetActiveScene().name;
        Instant = this;
        if (PlayerPrefs.GetString("PlayStyle") == "Freestyle")
        {
            Freestyle();
        }
        else if(PlayerPrefs.GetString("PlayStyle") == "Time")
        {
            TimeBase();
        }
        else if (PlayerPrefs.GetString("PlayStyle") == "Traffic")
        {
            Traffic();
        }
	}
    public Text FreestyleBestScore;
    void Freestyle()
    {
        timeLeft = 0;
        TimeMode = -1;
        if (PlayerPrefs.GetString(ScrenName + "FreestyleBestScore") == ""){
            PlayerPrefs.SetString(ScrenName + "FreestyleBestScore" , "00 Star in 00 : 00 time");
        }
        FreestyleBestScore.text = PlayerPrefs.GetString(ScrenName + "FreestyleBestScore", "00 Star in 00 : 00 time");
    }

    public Text TimeStartMsg;
    public Text TimeBestScore;
    int targt;
    void TimeBase()
    {
        targt = Random.Range(10, 20);
        int sec = (targt * 12) % 60;
        int mini = (targt * 12) / 60;
        TimeStartMsg.text = "You just collect maximum Star in " + mini.ToString("00") + ":" + sec.ToString("00") + " time";
        TimeBestScore.text = PlayerPrefs.GetString(ScrenName + "TimeBestScore", "00 Star in 00 : 00 time");
        timeLeft = targt * 12;
        TimeMode = 1;
    }

    public Text TrafficBestScore;
    void Traffic()
    {
        timeLeft = 0;
        TimeMode = -1;
        if (PlayerPrefs.GetString(ScrenName + "TrafficBestScore") == "")
        {
            PlayerPrefs.SetString(ScrenName + "TrafficBestScore", "00 Star in 00 : 00 time");
        }
        TrafficBestScore.text = PlayerPrefs.GetString(ScrenName + "TrafficBestScore", "00 Star in 00 : 00 time");
    }
	// Update is called once per frame
	void Update () {
        if (stop)
        {
            
            timeLeft -= Time.deltaTime*TimeMode;

            minutes = Mathf.Floor(timeLeft / 60);
            seconds = timeLeft % 60;
            if (seconds > 59) seconds = 59;
            if (minutes < 0)
            {
                stop = false;
                minutes = 0;
                seconds = 0;
                print("time puro-------"+PlayerPrefs.GetString("PlayStyle"));
                if (PlayerPrefs.GetString("PlayStyle") == "Time")
                {
                    
                    Time.timeScale = 0;
                    BestScore();
                 
                    //  AudioListener.enabled = false;
                    OverPanel.transform.parent.gameObject.SetActive(true);
                    GetComponent<AudioListener>().enabled = false;
                }
            }
            TimerText.text = string.Format("{0:0}:{1:00}", minutes, seconds);
        }
        
    }
    public void BestScore()
    {

        if (PlayerPrefs.GetString("PlayStyle") == "Freestyle")
        {           
            string[] besttime = PlayerPrefs.GetString(ScrenName+"FreestyleBestScore" ).Split(' ');
            if (int.Parse(besttime[0]) < TotalStar)
            {
                PlayerPrefs.SetString(ScrenName+"FreestyleBestScore", TotalStar.ToString("00") + " Star in " + minutes.ToString("00") + " : " + ((int)seconds).ToString("00") + " time");
            }
        }
        else if (PlayerPrefs.GetString("PlayStyle") == "Time")
        {
            float AvgStarTime = (targt * 12) / TotalStar;
            string[] besttime = TimeBestScore.text.ToString().Split(' ');
            float mini = float.Parse(besttime[3]);
            float sec = float.Parse(besttime[5]);
            float bestTime = ((mini * 60) + sec) / float.Parse(besttime[0]);
            if (AvgStarTime < bestTime || float.Parse(besttime[0])==0)
            {
                PlayerPrefs.SetString(ScrenName + "TimeBestScore", TotalStar.ToString("00") + " Star in "+ ((targt*12)/60).ToString("00") + " : "+ ((targt*12)%60).ToString("00") + " time");
            }
            OverPanel.transform.Find("CurrentScore").GetComponent<Text>().text = TotalStar.ToString("00") + " Star in " + ((targt * 12) / 60).ToString("00") + " : " + ((targt * 12) % 60).ToString("00") + " time";
            OverPanel.transform.Find("BestScore").GetComponent<Text>().text = PlayerPrefs.GetString(ScrenName + "TimeBestScore", "00 Star in 00 : 00 time");
        }
        else if (PlayerPrefs.GetString("PlayStyle") == "Traffic")
        {
            string[] besttime = PlayerPrefs.GetString(ScrenName + "TrafficBestScore").Split(' ');
            if (int.Parse(besttime[0]) < TotalStar)
            {
                PlayerPrefs.SetString(ScrenName + "TrafficBestScore", TotalStar.ToString("00") + " Star in " + minutes.ToString("00") + " : " + ((int)seconds).ToString("00") + " time");
            }
        }
    }
    public void AddStar()
    {
        TotalStar++;
        StarTex.text = TotalStar.ToString();
    }
    public void ShowStar(GameObject obj)
    {
        StartCoroutine(WaitStar(obj));
    }
    IEnumerator WaitStar(GameObject obj)
    {
        yield return new WaitForSeconds(10);
        obj.SetActive(true);
    }

    public void StratButton()
    {
        if (PlayerPrefs.GetString("PlayStyle") == "Freestyle")
        {
            timeLeft = 0;
            
        }
        else if (PlayerPrefs.GetString("PlayStyle") == "Time")
        {
            //TimeBase();
        }
        else if (PlayerPrefs.GetString("PlayStyle") == "Traffic")
        {
            //Traffic();
        }
        stop = true;
    }
}
