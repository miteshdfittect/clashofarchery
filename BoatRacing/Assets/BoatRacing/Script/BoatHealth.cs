﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class BoatHealth : MonoBehaviour {

    public static BoatHealth Instent;
    public GameObject HealthObject;
    public GameObject Traffic;
    Image HealthImage;
    float health;
    float imagevalue;
    void Start () {
        HealthImage = GetComponent<Image>();
        Instent = this;
        health = HealthImage.fillAmount * 100;
    }

    public void DownHealth(float Value)
    {
        Value /= 3;
        health = HealthImage.fillAmount*100;
        health -= Value;
        if (health < 20)
        {
            health = 20;
            Star.Instant.OverPanel.transform.parent.gameObject.SetActive(true);
        }
        
    }
    private void Update()
    {
        HealthImage.fillAmount = Mathf.Lerp(HealthImage.fillAmount, (health / 100),Time.deltaTime*2);
    }
}
