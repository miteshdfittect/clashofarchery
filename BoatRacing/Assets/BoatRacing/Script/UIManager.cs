﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour {

    public GameObject ButtonControl;
    public GameObject SteeringControl;
    public List<GameObject> StartPanel;
        
    // Use this for initialization
    void Start ()
    {
        
        if (PlayerPrefs.GetString("PlayStyle") == "Freestyle")
        {
            StartPanel[0].SetActive(true);
            StartCoroutine(StartWait());
        }
        else if (PlayerPrefs.GetString("PlayStyle") == "Time")
        {
            StartPanel[1].SetActive(true);
            StartCoroutine(StartWait());
        }
        else if (PlayerPrefs.GetString("PlayStyle") == "Traffic")
        {
            StartPanel[2].SetActive(true);
        }
        if (PlayerPrefs.GetString("ControlBy", "Button") == "Steering")
        {
            SteeringControl.SetActive(true);
            ButtonControl.SetActive(false);
        }
        else
        {
            SteeringControl.SetActive(false);
            ButtonControl.SetActive(true);
        }
	}
    IEnumerator StartWait()
    {
        yield return new WaitForEndOfFrame();
        BoatHealth.Instent.HealthObject.SetActive(false);
        BoatHealth.Instent.Traffic.SetActive(false);
    }
    public void BackToHome()
    {
        CanvasManager.Instent.PlayClikSound();
        AdsManager.Instent.ShowInterstitial();
        Time.timeScale = 1;
        //Invoke("BackHomeScene", 1);
        UnityEngine.SceneManagement.SceneManager.LoadScene("Home");
    }
    void BackHomeScene()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Home");
    }
    public void LoadSceen()
    {
        //CanvasManager.Instent.PlayClikSound();
        Time.timeScale = 1;
        AdsManager.Instent.ShowInterstitial();
        string name = PlayerPrefs.GetString("LoadedScene", "Home");
        UnityEngine.SceneManagement.SceneManager.LoadScene(name);
        //Invoke("Loadscene", 2);
        ////UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
    }
    void Loadscene()
    {
        string name= PlayerPrefs.GetString("LoadedScene", "Home");
        UnityEngine.SceneManagement.SceneManager.LoadScene(name);

        //UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
    }
    public void PlayPause(int i)
    {
        CanvasManager.Instent.PlayClikSound();
        Time.timeScale = i;
    }
}
