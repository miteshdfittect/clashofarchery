﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomeCameraControl : MonoBehaviour {

    // Use this for initialization
    public static HomeCameraControl Instent;
    public Transform[] BoatPositions;
    public int SelectedBoat;
	void Start () {
        Instent = this;
        SelectedBoat = PlayerPrefs.GetInt("SelectedBoat", 0);
	}
	
	// Update is called once per frame
	void Update () {

        transform.position = Vector3.Lerp(transform.position, BoatPositions[SelectedBoat].position, Time.deltaTime * 2);

	}
}
