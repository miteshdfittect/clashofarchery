﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CanvasManager : MonoBehaviour {

    public static CanvasManager Instent;
    // Use this for initialization
    public GameObject LoaderPanel;
    public GameObject LoadingPanel;
    public GameObject LockImage;
    public AudioSource Sound;
    public AudioSource Music;
    public AudioClip ClikSound;
    public Slider MusicSlider;
    public Slider SoundSlider;
    public Slider SensitiveSlider;
    public Dropdown Control;
    public GameObject[] DownPanel;
    public GameObject[] Boats;

    private void Awake()
    {
        PlayerPrefs.SetInt("Boat0Unlock", 1);
    }
    void Start () {
        Instent = this;
        PlayerPrefs.SetInt("Coin", 100000);
        Music.volume = PlayerPrefs.GetFloat("Music", 1);
        Sound.volume = PlayerPrefs.GetFloat("Sound", 1);
        if(MusicSlider!=null)
        MusicSlider.value = PlayerPrefs.GetFloat("Music", 1);
        if(SoundSlider!=null)
        SoundSlider.value = PlayerPrefs.GetFloat("Sound", 1);
        if (SensitiveSlider != null)
            SensitiveSlider.value = PlayerPrefs.GetFloat("Sensitive", 25);
        if (Control != null)
        {
            if ("Steering" == PlayerPrefs.GetString("ControlBy", "Button"))
            {
                Control.value = 0;
            }
            else
            {
                Control.value = 1;
            }
        }
    }
    public void PlayClikSound()
    {
        Sound.clip = ClikSound;
        if (PlayerPrefs.GetInt("AudioClip", 1) == 1)
        {
            Sound.Play();
        }
    }


    public void NextPrevious(int i)
    {
        PlayClikSound();
        HomeCameraControl.Instent.SelectedBoat += i;

        if (HomeCameraControl.Instent.SelectedBoat < 0)
        {
            HomeCameraControl.Instent.SelectedBoat = 4;
        }
        if (HomeCameraControl.Instent.SelectedBoat > 4)
        {
            HomeCameraControl.Instent.SelectedBoat = 0;
        }
        if (PlayerPrefs.GetInt("Boat" + HomeCameraControl.Instent.SelectedBoat.ToString() + "Unlock", 0) == 1)
        {
            LockImage.SetActive(false);
            DownPanel[0].SetActive(true);
            DownPanel[1].SetActive(false);
            PlayerPrefs.SetInt("SelectedBoat", HomeCameraControl.Instent.SelectedBoat);
        }
        else
        {
            LockImage.SetActive(true);
            DownPanel[1].SetActive(true);
            DownPanel[0].SetActive(false);
            switch (HomeCameraControl.Instent.SelectedBoat)
            {
                case 1:
                    DownPanel[1].transform.Find("AccessoriesButton").transform.GetChild(0).GetComponent<Text>().text = "15000";
                    break;
                case 2:
                    DownPanel[1].transform.Find("AccessoriesButton").transform.GetChild(0).GetComponent<Text>().text = "25000";
                    break;
                case 3:
                    DownPanel[1].transform.Find("AccessoriesButton").transform.GetChild(0).GetComponent<Text>().text = "50000";
                    break;
                case 4:
                    DownPanel[1].transform.Find("AccessoriesButton").transform.GetChild(0).GetComponent<Text>().text = "75000";
                    break;
            }
        }
        
    }
    public void BoatBuyButton()
    {
        if (PlayerPrefs.GetInt("Coin") >= int.Parse(DownPanel[1].transform.Find("AccessoriesButton").transform.GetChild(0).GetComponent<Text>().text))
        {
            PlayerPrefs.SetInt("Coin", PlayerPrefs.GetInt("Coin") - int.Parse(DownPanel[1].transform.Find("AccessoriesButton").transform.GetChild(0).GetComponent<Text>().text));
            PlayerPrefs.SetInt("Boat" + HomeCameraControl.Instent.SelectedBoat.ToString() + "Unlock", 1);
            LockImage.SetActive(false);
            DownPanel[0].SetActive(true);
            DownPanel[1].SetActive(false);
            PlayerPrefs.SetInt("SelectedBoat", HomeCameraControl.Instent.SelectedBoat);
            MessagePanel.Instent.Show("Congratulation !! \n your have unlock new Boat", "Okay");
        }
        else
        {
            MessagePanel.Instent.Show("Sorry, You dont have enough coin", "Get Coin", () =>
            {
                gameObject.transform.root.Find("ShopPanel").gameObject.SetActive(true);
            });
        }
    }
    public void SetTrue(GameObject obj)
    {
        obj.SetActive(true);
    }
    public void SetFalse(GameObject obj)
    {
        obj.SetActive(false);
    }
    public void Togal(GameObject obj)
    {
        obj.SetActive(!obj.activeSelf);
    }
    
    public void SetVolume()
    {
        if (EventSystem.current.currentSelectedGameObject != null)
        {
            if (EventSystem.current.currentSelectedGameObject.name == "MusicSlider")
            {
                float vl = EventSystem.current.currentSelectedGameObject.GetComponent<Slider>().value;
                PlayerPrefs.SetFloat("Music", vl);
                Music.volume = vl;
            }
            else if (EventSystem.current.currentSelectedGameObject.name == "SoundSlider")
            {
                float vl = EventSystem.current.currentSelectedGameObject.GetComponent<Slider>().value;
                PlayerPrefs.SetFloat("Sound", vl);
                Sound.volume = vl;
            }
        }
    }
    public void SetSensitive()
    {
        if (EventSystem.current.currentSelectedGameObject != null)
        {            
            float vl = EventSystem.current.currentSelectedGameObject.GetComponent<Slider>().value;
            PlayerPrefs.SetFloat("Sensitive", vl);
        }
    }
        public void ControlBy()
    {
        if (Control.value == 1)
        {
            PlayerPrefs.SetString("ControlBy", "Button");
        }
        if (Control.value == 0)
        {
            PlayerPrefs.SetString("ControlBy", "Steering");
        }
    }
    public void ShowLeaderbord()
    {
        LeaderBoard.Instent.Show();
    }
    public void LevelSelection(string LevelName)
    {
        PlayClikSound();
        
        if (LevelName == "Freestyle" || LevelName == "Time" || LevelName == "Traffic")
        {            
            PlayerPrefs.SetString("PlayStyle", LevelName);
        }
        else
        {
            AdsManager.Instent.ShowInterstitial();
            PlayerPrefs.SetString("LoadedScene", LevelName);
            UnityEngine.SceneManagement.SceneManager.LoadScene(LevelName);
        }
        
    }
    public void LockUnlock()
    {
        PlayClikSound();
        if (EventSystem.current.currentSelectedGameObject.name == "AccessoriesButton")
        {
            if (PlayerPrefs.GetInt("BoatAccessories" + HomeCameraControl.Instent.SelectedBoat + "1", 0) == 1)
            {
                GameObject.Find("AccessoriesPanel").transform.GetChild(0).transform.Find("Coin").gameObject.SetActive(false);
            }
            else
            {
                GameObject.Find("AccessoriesPanel").transform.GetChild(0).transform.Find("Coin").gameObject.SetActive(true);
            }
            if (PlayerPrefs.GetInt("BoatAccessories" + HomeCameraControl.Instent.SelectedBoat + "2", 0) == 1)
            {
                GameObject.Find("AccessoriesPanel").transform.GetChild(1).transform.Find("Coin").gameObject.SetActive(false);
            }
            else
            {
                GameObject.Find("AccessoriesPanel").transform.GetChild(1).transform.Find("Coin").gameObject.SetActive(true);
            }
            if (PlayerPrefs.GetInt("BoatAccessories" + HomeCameraControl.Instent.SelectedBoat + "3", 0) == 1)
            {
                GameObject.Find("AccessoriesPanel").transform.GetChild(2).transform.Find("Coin").gameObject.SetActive(false);
            }
            else
            {
                GameObject.Find("AccessoriesPanel").transform.GetChild(2).transform.Find("Coin").gameObject.SetActive(true);
            }
        }
        else if (EventSystem.current.currentSelectedGameObject.name == "EngineButton")
        {
            if (PlayerPrefs.GetInt("BoatEngine" + HomeCameraControl.Instent.SelectedBoat + "1", 1) == 1)
            {
                GameObject.Find("EnginePanel").transform.GetChild(0).transform.Find("Coin").gameObject.SetActive(false);
            }

            if (PlayerPrefs.GetInt("BoatEngine" + HomeCameraControl.Instent.SelectedBoat + "2", 0) == 1)
            {
                GameObject.Find("EnginePanel").transform.GetChild(1).transform.Find("Coin").gameObject.SetActive(false);
            }
            else
            {
                GameObject.Find("EnginePanel").transform.GetChild(1).transform.Find("Coin").gameObject.SetActive(true);
            }
            if (PlayerPrefs.GetInt("BoatEngine" + HomeCameraControl.Instent.SelectedBoat + "3", 0) == 1)
            {
                GameObject.Find("EnginePanel").transform.GetChild(2).transform.Find("Coin").gameObject.SetActive(false);
            }
            else
            {
                GameObject.Find("EnginePanel").transform.GetChild(2).transform.Find("Coin").gameObject.SetActive(true);
            }
            if (PlayerPrefs.GetInt("BoatEngine" + HomeCameraControl.Instent.SelectedBoat + "4", 0) == 1)
            {
                GameObject.Find("EnginePanel").transform.GetChild(3).transform.Find("Coin").gameObject.SetActive(false);
            }
            else
            {
                GameObject.Find("EnginePanel").transform.GetChild(3).transform.Find("Coin").gameObject.SetActive(true);
            }
            if (PlayerPrefs.GetInt("BoatEngine" + HomeCameraControl.Instent.SelectedBoat + "5", 0) == 1)
            {
                GameObject.Find("EnginePanel").transform.GetChild(4).transform.Find("Coin").gameObject.SetActive(false);
            }
            else
            {
                GameObject.Find("EnginePanel").transform.GetChild(4).transform.Find("Coin").gameObject.SetActive(true);
            }
        }
        else if (EventSystem.current.currentSelectedGameObject.name == "UpgradesButton")
        {
            upgrad();

        }
    }
    public void BuyButton()
    {
        PlayClikSound();
        if (EventSystem.current.currentSelectedGameObject.transform.parent.name == "AccessoriesPanel")
        {
            if (EventSystem.current.currentSelectedGameObject.transform.Find("Coin").gameObject.activeSelf)
            {
                if (PlayerPrefs.GetInt("Coin") >= int.Parse(EventSystem.current.currentSelectedGameObject.transform.Find("Coin").GetComponent<Text>().text))
                {
                    PlayerPrefs.SetInt("Coin", PlayerPrefs.GetInt("Coin") - int.Parse(EventSystem.current.currentSelectedGameObject.transform.Find("Coin").GetComponent<Text>().text));
                    PlayerPrefs.SetInt("BoatAccessories" + HomeCameraControl.Instent.SelectedBoat + EventSystem.current.currentSelectedGameObject.name, 1);
                    EventSystem.current.currentSelectedGameObject.transform.Find("Coin").gameObject.SetActive(false);
                    PlayerPrefs.SetInt("BoatAccessories" + HomeCameraControl.Instent.SelectedBoat, int.Parse(EventSystem.current.currentSelectedGameObject.name));
                    Boats[HomeCameraControl.Instent.SelectedBoat].transform.Find("Accessories").GetComponent<Accessories>().Access();
                }
                else
                {
                    MessagePanel.Instent.Show("Sorry, You dont have enough coin", "Get Coin", () =>
                    {
                        gameObject.transform.root.Find("ShopPanel").gameObject.SetActive(true);
                    });
                }
            }
            else
            {
                PlayerPrefs.SetInt("BoatAccessories" + HomeCameraControl.Instent.SelectedBoat + EventSystem.current.currentSelectedGameObject.name, 1);
                PlayerPrefs.SetInt("BoatAccessories" + HomeCameraControl.Instent.SelectedBoat, int.Parse(EventSystem.current.currentSelectedGameObject.name));
                Boats[HomeCameraControl.Instent.SelectedBoat].transform.Find("Accessories").GetComponent<Accessories>().Access();
            }
        }
        else if (EventSystem.current.currentSelectedGameObject.transform.parent.name == "EnginePanel")
        {
            if (EventSystem.current.currentSelectedGameObject.transform.Find("Coin").gameObject.activeSelf)
            {
                if (PlayerPrefs.GetInt("Coin") >= int.Parse(EventSystem.current.currentSelectedGameObject.transform.Find("Coin").GetComponent<Text>().text))
                {
                    PlayerPrefs.SetInt("Coin", PlayerPrefs.GetInt("Coin") - int.Parse(EventSystem.current.currentSelectedGameObject.transform.Find("Coin").GetComponent<Text>().text));
                    PlayerPrefs.SetInt("BoatEngine" + HomeCameraControl.Instent.SelectedBoat + EventSystem.current.currentSelectedGameObject.name, 1);
                    EventSystem.current.currentSelectedGameObject.transform.Find("Coin").gameObject.SetActive(false);
                    PlayerPrefs.SetInt("BoatEngine" + HomeCameraControl.Instent.SelectedBoat, int.Parse(EventSystem.current.currentSelectedGameObject.name));
                    Boats[HomeCameraControl.Instent.SelectedBoat].transform.Find("Motor_L").GetComponent<Engine>().Eng();
                    Boats[HomeCameraControl.Instent.SelectedBoat].transform.Find("Motor_R").GetComponent<Engine>().Eng();
                }
                else
                {
                    MessagePanel.Instent.Show("Sorry, You dont have enough coin", "Get Coin", () =>
                    {
                        gameObject.transform.root.Find("ShopPanel").gameObject.SetActive(true);
                    });
                }
            }
            else
            {
                PlayerPrefs.SetInt("BoatEngine" + HomeCameraControl.Instent.SelectedBoat + EventSystem.current.currentSelectedGameObject.name, 1);
                PlayerPrefs.SetInt("BoatEngine" + HomeCameraControl.Instent.SelectedBoat, int.Parse(EventSystem.current.currentSelectedGameObject.name));
                print("----------------->-------->" + HomeCameraControl.Instent.SelectedBoat);
                Boats[HomeCameraControl.Instent.SelectedBoat].transform.Find("Motor_L").GetComponent<Engine>().Eng();
                Boats[HomeCameraControl.Instent.SelectedBoat].transform.Find("Motor_R").GetComponent<Engine>().Eng();
            }
        }
        else if (EventSystem.current.currentSelectedGameObject.transform.parent.name == "UpgradesPanel")
        {
            if (EventSystem.current.currentSelectedGameObject.transform.Find("Coin").gameObject.activeSelf)
            {
                if (PlayerPrefs.GetInt("Coin") >= int.Parse(EventSystem.current.currentSelectedGameObject.transform.Find("Coin").GetComponent<Text>().text.ToString()))
                {
                    PlayerPrefs.SetInt("Coin", PlayerPrefs.GetInt("Coin") - int.Parse(EventSystem.current.currentSelectedGameObject.transform.Find("Coin").GetComponent<Text>().text));
                    PlayerPrefs.SetInt(EventSystem.current.currentSelectedGameObject.name + HomeCameraControl.Instent.SelectedBoat, PlayerPrefs.GetInt(EventSystem.current.currentSelectedGameObject.name + HomeCameraControl.Instent.SelectedBoat, 0) + 1);
                    upgrad();
                }
                else
                {
                    MessagePanel.Instent.Show("Sorry, You dont have enough coin", "Get Coin", () =>
                    {
                        gameObject.transform.root.Find("ShopPanel").gameObject.SetActive(true);
                    });
                }

            }
        }
    }
    public GameObject SpeedObj ;
    public GameObject TurningObj;
    public GameObject AcceleObj;
    void upgrad()
    {
        int speed = PlayerPrefs.GetInt("SpeedUpgrade" + HomeCameraControl.Instent.SelectedBoat, 0);
        print("Speed is........................."+speed);
        int TurningFactor = PlayerPrefs.GetInt("TurningFactorUpgrade" + HomeCameraControl.Instent.SelectedBoat, 0);
        int Acceleration = PlayerPrefs.GetInt("AccelerationTorqueUpgrade" + HomeCameraControl.Instent.SelectedBoat, 0);
        
        
        print("Speed------>" + SpeedObj);
        //-------------------Speed--------------------------// 
        SpeedObj.transform.Find("ParRate").GetComponent<Text>().text = speed + "/10";
        SpeedObj.transform.Find("UpGrdImg").Find("GRN").GetComponent<RectTransform>().anchoredPosition = new Vector3(-150 + ((speed+1) * 15), 0, 0);
        SpeedObj.transform.Find("UpGrdImg").Find("GRN").GetComponent<RectTransform>().sizeDelta = new Vector2((speed+1) * 15, 0);
        SpeedObj.transform.Find("Coin").GetComponent<Text>().text = ((speed + 1) * 1000).ToString();
        SpeedObj.transform.Find("LNo").GetChild(0).GetComponent<Text>().text = ((speed + 1) / 2).ToString();
        //------------------Acceleration-------------------//
        AcceleObj.transform.Find("ParRate").GetComponent<Text>().text = Acceleration + "/10";
        AcceleObj.transform.Find("UpGrdImg").Find("GRN").GetComponent<RectTransform>().anchoredPosition = new Vector3(-100 + ((Acceleration+1) * 10), 0, 0);
        AcceleObj.transform.Find("UpGrdImg").Find("GRN").GetComponent<RectTransform>().sizeDelta = new Vector2(((Acceleration + 1) * 10)+20, 0);
        AcceleObj.transform.Find("Coin").GetComponent<Text>().text = ((Acceleration + 1) * 1000).ToString();
        AcceleObj.transform.Find("LNo").GetChild(0).GetComponent<Text>().text = ((Acceleration + 1) / 2).ToString();
        //------------------TurningFactor-------------------//
        TurningObj.transform.Find("ParRate").GetComponent<Text>().text = TurningFactor + "/10";
        TurningObj.transform.Find("UpGrdImg").Find("GRN").GetComponent<RectTransform>().anchoredPosition = new Vector3(-100 + ((TurningFactor+1) * 10), 0, 0);
        TurningObj.transform.Find("UpGrdImg").Find("GRN").GetComponent<RectTransform>().sizeDelta = new Vector2(((TurningFactor + 1) * 10)+10, 0);
        TurningObj.transform.Find("Coin").GetComponent<Text>().text = ((TurningFactor + 1) * 1000).ToString();
        TurningObj.transform.Find("LNo").GetChild(0).GetComponent<Text>().text = ((TurningFactor + 1) / 2).ToString();

        if (speed >= 9)
        {
            SpeedObj.transform.Find("ParRate").GetComponent<Text>().text = "10/10";
            SpeedObj.transform.Find("Coin").gameObject.SetActive(false);
        }
        else
        {
            SpeedObj.transform.Find("Coin").gameObject.SetActive(true);
        }
        if (TurningFactor >= 9)
        {
            TurningObj.transform.Find("ParRate").GetComponent<Text>().text = "10/10";
            TurningObj.transform.Find("Coin").gameObject.SetActive(false);
        }
        else
        {
            TurningObj.transform.Find("Coin").gameObject.SetActive(true);
        }
        if (Acceleration >= 9)
        {
            AcceleObj.transform.Find("ParRate").GetComponent<Text>().text = "10/10";
            AcceleObj.transform.Find("Coin").gameObject.SetActive(false);
        }
        else
        {
            AcceleObj.transform.Find("Coin").gameObject.SetActive(true);
        }
    }
    public void Share()
    {
        //execute the below lines if being run on a Android device
#if UNITY_ANDROID
        //Reference of AndroidJavaClass class for intent
        AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
        //Reference of AndroidJavaObject class for intent
        AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");
        //call setAction method of the Intent object created
        intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
        //set the type of sharing that is happening
        intentObject.Call<AndroidJavaObject>("setType", "text/plain");
        //add data to be passed to the other activity i.e., the data to be sent
        intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_SUBJECT"), "Boat Racing");
        intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), "No one can stop you being a racer!\nDownload this game\n https://play.google.com/store/apps/details?id=" + Application.identifier);
        //get the current activity
        AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");
        //start the activity by sending the intent data
        currentActivity.Call("startActivity", intentObject);
#endif
    }

    public void Rate()
    {
        Application.OpenURL("https://play.google.com/store/apps/details?id=" + Application.identifier);
    }
}
