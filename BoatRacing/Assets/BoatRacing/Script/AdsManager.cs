﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AudienceNetwork;
using GoogleMobileAds.Api;


public class AdsManager : MonoBehaviour {

    public static AdsManager Instent;
    //---------FB Banner--------------
    [SerializeField] public string FBBannerAdsID;
    private AdView FBadView;

    //---------FB interstitialAd------
    [SerializeField] public string FBInterstitialAdsID;
    private AudienceNetwork.InterstitialAd FBinterstitialAd;
    private bool FBInsterstitialisLoaded;


    //---------Google Banner--------------
    [SerializeField] private string GooglebannerViewID;
    private BannerView GooglebannerView;

    //---------Google interstitialAd------
    [SerializeField] public string GoogleInterstitialID;
    private GoogleMobileAds.Api.InterstitialAd Googleinterstitial;

    //-------------------- Video Ad------------------------------
    [SerializeField] public string GoogleRewardBasedVideoID;
    private GoogleMobileAds.Api.RewardBasedVideoAd rewardBasedVideo;


    IEnumerator Start () {

        Instent = this;
        yield return new WaitForSeconds(1);
        if (PlayerPrefs.GetInt("ad_type", 0) == 0)
        {
            //Google Ad
            GoogleRequestInterstitial();

        }
        else
        {
            //FB Ads
            FBLoadInterstitial();
            FBLoadBanner();
        }


        this.rewardBasedVideo = GoogleMobileAds.Api.RewardBasedVideoAd.Instance;

        this.rewardBasedVideo.OnAdRewarded += this.HandleRewardBasedVideoRewarded;
        this.rewardBasedVideo.OnAdClosed += this.HandleRewardBasedVideoClosed;


        GooglRequestRewardBasedVideo();

        if (PlayerPrefs.GetInt("is_home_ad", 1) == 1 && !LeaderBoard.FastTimeOpen)
        {
            LeaderBoard.FastTimeOpen = true;
            Invoke("ShowInterstitial",1);
        }
    }

    void OnDestroy()
    {
        DestroyBanner();
    }


    //---------------------------------------------------------------------------------------------------------------------------------------------
    public void FBLoadBanner()
    {
        if (this.FBadView)
        {
            this.FBadView.Dispose();
        }

        this.FBadView = new AdView(FBBannerAdsID, AudienceNetwork.AdSize.BANNER_HEIGHT_50);
        this.FBadView.Register(this.gameObject);

        // Set delegates to get notified on changes or when the user interacts with the ad.
        this.FBadView.AdViewDidLoad = (delegate () {
            Debug.Log("Banner loaded.");
        });
        FBadView.AdViewDidFailWithError = (delegate (string error) {
            Debug.Log("Banner failed to load with error: " + error);
        });
        FBadView.AdViewWillLogImpression = (delegate () {
            Debug.Log("Banner logged impression.");
        });
        FBadView.AdViewDidClick = (delegate () {
            Debug.Log("Banner clicked.");
        });

        // Initiate a request to load an ad.
        FBadView.LoadAd();
    }

    #region FaceBookInterstitial

    public void FBLoadInterstitial()
    {
        AudienceNetwork.InterstitialAd interstitialAd = new AudienceNetwork.InterstitialAd(FBInterstitialAdsID);
        this.FBinterstitialAd = interstitialAd;
        this.FBinterstitialAd.Register(this.gameObject);

        // Set delegates to get notified on changes or when the user interacts with the ad.
        this.FBinterstitialAd.InterstitialAdDidLoad = (delegate () {
            Debug.Log("Interstitial ad loaded.");
            this.FBInsterstitialisLoaded = true;
        });
        interstitialAd.InterstitialAdDidFailWithError = (delegate (string error) {
            Debug.Log("Interstitial ad failed to load with error: " + error);
        });
        interstitialAd.InterstitialAdWillLogImpression = (delegate () {
            Debug.Log("Interstitial ad logged impression.");
        });
        interstitialAd.InterstitialAdDidClick = (delegate () {
            Debug.Log("Interstitial ad clicked.");
        });

        // Initiate the request to load the ad.
        this.FBinterstitialAd.LoadAd();
    }
    #endregion

    //-----------------------------------------------------------------------------------------------------------------------------------------------------------------
    private GoogleMobileAds.Api.AdRequest CreateAdRequest()
    {
        return new GoogleMobileAds.Api.AdRequest.Builder()
            .AddKeyword("game")
            .Build();
    }
    private void GoogleRequestBanner()
    {
               

        // Clean up banner ad before creating a new one.
        if (this.GooglebannerView != null)
        {
            this.GooglebannerView.Destroy();
        }

        // Create a 320x50 banner at the top of the screen.
        this.GooglebannerView = new BannerView(GooglebannerViewID, GoogleMobileAds.Api.AdSize.SmartBanner, GoogleMobileAds.Api.AdPosition.Bottom);
        
        // Load a banner ad.
        this.GooglebannerView.LoadAd(this.CreateAdRequest());

    }

    private void GoogleRequestInterstitial()
    {


        // Clean up interstitial ad before creating a new one.
        if (this.Googleinterstitial != null)
        {
            this.Googleinterstitial.Destroy();
        }

        // Create an interstitial.
        this.Googleinterstitial = new GoogleMobileAds.Api.InterstitialAd(GoogleInterstitialID);

        // Load an interstitial ad.
        this.Googleinterstitial.LoadAd(this.CreateAdRequest());
    }

    private void GooglRequestRewardBasedVideo()
    {
        this.rewardBasedVideo.LoadAd(this.CreateAdRequest(), GoogleRewardBasedVideoID);
    }

    public void GooglShowRewardBasedVideo()
    {
        StartCoroutine(checkInternetConnection());
    }
    IEnumerator checkInternetConnection()
    {
        WWW www = new WWW("http://google.com");
        yield return www;
        if (www.error != null)
        {
            CanvasManager.Instent.LoaderPanel.SetActive(false);
            MessagePanel.Instent.Show("You have no Internet Connection\n Check your Internet Connection!!!", "Okay");
        }
        else
        {
            if (this.rewardBasedVideo.IsLoaded())
            {
                CanvasManager.Instent.LoaderPanel.SetActive(false);
                this.rewardBasedVideo.Show();
            }
            else
            {
                StartCoroutine(VideoRecst());
            }
        }
    }
    int VideoCount = 0;
    IEnumerator VideoRecst()
    {
        this.rewardBasedVideo.LoadAd(this.CreateAdRequest(), GoogleRewardBasedVideoID);
        yield return new WaitForSeconds(1);
        yield return rewardBasedVideo.IsLoaded();
        if (this.rewardBasedVideo.IsLoaded())
        {
            CanvasManager.Instent.LoaderPanel.SetActive(false);
            this.rewardBasedVideo.Show();
            VideoCount = 0;
        }
        else if (VideoCount < 15)
        {
            VideoCount++;
            StartCoroutine(VideoRecst());
        }
        else
        {
            VideoCount = 0;
            CanvasManager.Instent.LoaderPanel.SetActive(false);
            MessagePanel.Instent.Show("Reward video ad is not ready yet", "Retry", "Close", () =>
            {
                StartCoroutine(checkInternetConnection());
            });
        }

    }

    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public void ShowInterstitial()
    {
        if (PlayerPrefs.GetInt("ad_type", 0) == 0)
        {
            //Google Ad
            if (this.Googleinterstitial.IsLoaded())
            {
                this.Googleinterstitial.Show();
            }
            else
            {
                MonoBehaviour.print("Interstitial is not ready yet");
            }
            GoogleRequestInterstitial();
        }
        else
        {
            //FB Ads
            if (this.FBInsterstitialisLoaded)
            {
                this.FBinterstitialAd.Show();
                this.FBInsterstitialisLoaded = false;
            }
            else
            {

            }
            FBLoadInterstitial();
        }

        
    }

    public void ShowBanner()
    {
        if (PlayerPrefs.GetInt("ad_type", 0) == 0)
        {
            //Google Ad
            GoogleRequestBanner();
        }
        else
        {
            //FB Ad
            this.FBadView.Show(AudienceNetwork.AdPosition.BOTTOM);
        }
    }

    public void DestroyBanner()
    {
        if (this.FBadView)
        {
            this.FBadView.Dispose();
        }
        Debug.Log("AdViewTest was destroyed!");
        if (this.GooglebannerView != null)
        {
            this.GooglebannerView.Destroy();
        }
    }
    #region RewardBasedVideo callback handlers


    public void HandleRewardBasedVideoClosed(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoClosed event received");
        GooglRequestRewardBasedVideo();
    }

    public void HandleRewardBasedVideoRewarded(object sender, Reward args)
    {               
        MessagePanel.Instent.Show("Congratulations !!! you get 500 Reward", "Collect Reward", () => {
            PlayerPrefs.SetInt("Coin", PlayerPrefs.GetInt("Coin") + 500);
        });
    }

    #endregion
}
