﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;
using System.IO;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Linq;

public class GameAPI : MonoBehaviour
{

    public GameObject MoreGamePrefabe;
    public GameObject MoreGameParent;

    string VersionURL;
    string TokenURL;
    string MoreGameURL;
    int GameID = 2;


    // Use this for initialization
    void Start()
    {
        VersionURL = "http://phpstack-184762-608677.cloudwaysapps.com/service/version/" + GameID;
        TokenURL = "http://phpstack-184762-608677.cloudwaysapps.com/service/token/" + GameID;
        MoreGameURL = "http://phpstack-184762-608677.cloudwaysapps.com/service/more_game/" + GameID + "/1";
        //DontDestroyOnLoad(this.gameObject);

        //----call Version API ----------
        print("version----------------1111---->" + PlayerPrefs.GetString("version", "1.0"));
        StartCoroutine(VersionAPIcalling());
        //----Call MoreGame API --------
        StartCoroutine(MoreGameAPIcalling());
        //----Call Token API -------
        if (PlayerPrefs.GetInt("FistTime", 0) == 0)
        {
            StartCoroutine(TokanAPIcalling());
        }
    }
    void OnApplicationPause(bool pauseStatus)
    {
        if (PlayerPrefs.GetInt("force_update", 0) == 1)
        {
            StartCoroutine(VersionAPIcalling());
        }
    }

    #region VersionAPI
    public IEnumerator VersionAPIcalling()
    {
        WWW www = new WWW(VersionURL);
        yield return www;

        if (www.error == null)
        {
            Processjson(www.text);
        }
        float version = float.Parse(PlayerPrefs.GetString("version", "1.0"));
        print("-----"+PlayerPrefs.GetString("version", "1.0")+"------" + version);
        if (version > float.Parse(Application.version))
        {
            if (PlayerPrefs.GetInt("force_update", 0) == 1)
            {
                MessagePanel.Instent.Show("Update is available\nDownload Now", "Update", () =>
                {
                    Application.OpenURL("https://play.google.com/store/apps/details?id=" + Application.identifier);
                });
            }
            else
            {
                MessagePanel.Instent.Show("Update is available\nDownload Now", "Update", "Not Now", () =>
                 {
                     Application.OpenURL("https://play.google.com/store/apps/details?id=" + Application.identifier);
                 });
            }
        }
    }

    public void Processjson(string JsonString)
    {
        JsonData jsonvale = JsonMapper.ToObject(JsonString);
        PlayerPrefs.SetString("status", jsonvale["status"].ToString());
        PlayerPrefs.SetString("version", jsonvale["version"].ToString());
        print("----" + jsonvale["version"].ToString());
        PlayerPrefs.SetInt("ad_type", int.Parse(jsonvale["ad_type"].ToString()));       
        PlayerPrefs.SetInt("is_home_ad",int.Parse(jsonvale["is_home_ad"].ToString()));
        PlayerPrefs.SetInt("force_update", int.Parse(jsonvale["force_update"].ToString()));
    }
    #endregion



    #region TokanAPI
    public IEnumerator TokanAPIcalling()
    {
        WWW www = new WWW(VersionURL);
        yield return www;

        if (www.error == null)
        {
            JsonData jsonvale = JsonMapper.ToObject(www.text);            
            PlayerPrefs.SetInt("FistTime", 1);    
        }
    }
    #endregion



    #region MoreGame
    public IEnumerator MoreGameAPIcalling()
    {
        WWW www = new WWW(MoreGameURL);
        yield return www;

        if (www.error == null)
        {
            JsonData jsonvale = JsonMapper.ToObject(www.text);

            bool sts = (bool)jsonvale["status"];
            if (sts)
            {
                for (int i = 0; i < jsonvale["games"].Count; i++)
                {
                    StartCoroutine(StoreIcon(
                        jsonvale["games"][i]["icon"].ToString(),
                        int.Parse(jsonvale["games"][i]["game_id"].ToString()),
                        jsonvale["games"][i]["game"].ToString(),
                        jsonvale["games"][i]["package_name"].ToString()
                        ));

                    if (i == (jsonvale["games"].Count - 1))
                    {
                        StartCoroutine(ShowMoreGame());
                    }
                }

            }
        }
        else
        {
            StartCoroutine(ShowMoreGame());
        }

    }
    public IEnumerator StoreIcon(string Url,int id,string Name,string PackageName)
    {
        Texture2D texture;
        WWW www = new WWW(Url);
        yield return www;
        texture = new Texture2D(www.texture.height, www.texture.width);
        www.LoadImageIntoTexture(texture);
        string path = Application.persistentDataPath +"/"+ (id + "-"+ Name +"-"+ PackageName).ToString();
        File.WriteAllBytes(path, texture.EncodeToPNG());
        
    }


    IEnumerator ShowMoreGame()
    {
        yield return new WaitForSeconds(0.5f);
        DirectoryInfo dir = new DirectoryInfo(Application.persistentDataPath);
        FileInfo[] info = dir.GetFiles().ToArray();
        if (info.Length > 0)
        {
            for (int cou = 0; cou < info.Length; cou++)
            {
                Texture2D newTexture = new Texture2D(512, 512);
                newTexture.LoadImage(File.ReadAllBytes(Application.persistentDataPath + "/" + info[cou].Name));
                string detai = info[cou].Name;
                string[] detail = detai.Split('-');
                Sprite NewSprite = Sprite.Create(newTexture, new Rect(0, 0, newTexture.width, newTexture.height), new Vector2(0, 0));
                GameObject MoreGame = Instantiate(MoreGamePrefabe, MoreGameParent.transform);                
                MoreGame.transform.Find("Name").GetComponent<Text>().text = detail[1];
                MoreGame.transform.Find("Icon").GetComponent<Image>().sprite = NewSprite;
                Button btn = MoreGame.transform.Find("Button").GetComponent<Button>();

                if (IsAppInstalled(detail[2]))
                {
                    MoreGame.GetComponent<Button>().interactable = false;
                    MoreGame.transform.Find("Download").GetComponent<Text>().text = "Installed";
                }
                else
                {
                    btn.onClick.AddListener(() =>
                    {
                        Application.OpenURL("https://play.google.com/store/apps/details?id=" + detail[2]);
                    });
                }
                
                if (IsAppInstalled(detail[2]) && PlayerPrefs.GetInt("App" + cou, 0) == 0)
                {
                    PlayerPrefs.SetInt("App" + cou, 1);
                    MessagePanel.Instent.Show("Congratulations !!!\n you download " + detail[1] + " successfully and you earn 500 Coin", "Collect", () => {
                        PlayerPrefs.SetInt("Coin", PlayerPrefs.GetInt("Coin") + 500);
                    });
                }
            }

        }
    }
    
    public bool IsAppInstalled(string bundleID)
    {
        #if UNITY_ANDROID && !UNITY_EDITOR
                AndroidJavaClass up = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                AndroidJavaObject ca = up.GetStatic<AndroidJavaObject>("currentActivity");
                AndroidJavaObject packageManager = ca.Call<AndroidJavaObject>("getPackageManager");
                Debug.Log(" ********LaunchOtherApp ");
                AndroidJavaObject launchIntent = null;
                //if the app is installed, no errors. Else, doesn't get past next line
                try
                {
                    launchIntent = packageManager.Call<AndroidJavaObject>("getLaunchIntentForPackage", bundleID);            
                }
                catch
                {
            
                }
                if (launchIntent == null)
                    return false;
                return true;
        #else
                return true;
        #endif
    }
    #endregion

    void OnApplicationFocus(bool focusStatus)
    {
        DirectoryInfo dir = new DirectoryInfo(Application.persistentDataPath);
        FileInfo[] info = dir.GetFiles().ToArray();
        if (info.Length > 0)
        {
            for (int cou = 0; cou < info.Length; cou++)
            {
                string detai = info[cou].Name;
                string[] detail = detai.Split('-');
                if (IsAppInstalled(detail[2]) && PlayerPrefs.GetInt("App" + cou, 0) == 0)
                {
                    PlayerPrefs.SetInt("App" + cou, 1);
                    MessagePanel.Instent.Show("Congratulations !!!\n you download " + detail[1] + " successfully and you earn 500 Stone", "Collect", () => {
                        PlayerPrefs.SetInt("Coin", PlayerPrefs.GetInt("Coin") + 500);
                    });
                }
            }

        }
    }

}
