﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

#region Delegates
public delegate void MouseMoved(float xMovement, float yMovement);
#endregion
public class InputManager : MonoBehaviour
{
    #region Private References

    private float _xMovement;
    private float _yMovement;
    #endregion

    #region Events
    public static event MouseMoved MouseMoved;
    #endregion

    #region Event Invoker Methods
    private static void OnMouseMoved(float xmovement, float ymovement)
    {
        var handler = MouseMoved;
        if (handler != null) handler(xmovement, ymovement);
    }
    #endregion

    #region Private Methods

    private void InvokeActionOnInput()
    {

#if UNITY_EDITOR
        if (Input.GetMouseButton(0) && !EventSystem.current.IsPointerOverGameObject())
        {
            _xMovement = Input.GetAxis("Mouse X");
            _yMovement = Input.GetAxis("Mouse Y");
#endif

#if UNITY_ANDROID && !UNITY_EDITOR
            if (Input.GetMouseButton(0) && !EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))
            {
            _xMovement = Input.GetTouch(0).deltaPosition.x*0.05f;
            _yMovement = Input.GetTouch(0).deltaPosition.y*0.05f;
#endif
            OnMouseMoved(_xMovement, _yMovement);
        }

        //print("_xMovement----------->" + _xMovement);

        //print("_yMovement>" + _yMovement);
        //if (Input.GetMouseButtonUp(0))
        //{
        //    _xMovement = 0;
        //    _yMovement = 0;
        //}
    }
    #endregion

    #region Unity CallBacks
   
    void Update()
    {
        InvokeActionOnInput();
    }

    #endregion


}
