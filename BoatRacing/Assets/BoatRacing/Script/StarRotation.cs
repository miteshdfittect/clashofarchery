﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarRotation : MonoBehaviour {


    
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(Vector3.up  );
        
    }
    private void OnTriggerEnter(Collider other)
    {
       
        if (PlayerPrefs.GetString("PlayStyle") == "Freestyle" && other.tag=="Player")
        {
            Star.Instant.ShowStar(this.gameObject);
            Star.Instant.AddStar();
            Star.Instant.BestScore();
            gameObject.SetActive(false);
            PlayerPrefs.SetInt("Coin", PlayerPrefs.GetInt("Coin") + 10);
        }
        else if (PlayerPrefs.GetString("PlayStyle") == "Time" && other.tag == "Player")
        {
            Star.Instant.ShowStar(this.gameObject);
            Star.Instant.AddStar();
            gameObject.SetActive(false);
            PlayerPrefs.SetInt("Coin", PlayerPrefs.GetInt("Coin") + 15);
        }
        else if (PlayerPrefs.GetString("PlayStyle") == "Traffic" && other.tag == "Player")
        {
            Star.Instant.ShowStar(this.gameObject);
            Star.Instant.AddStar();
            Star.Instant.BestScore();
            gameObject.SetActive(false);
            PlayerPrefs.SetInt("Coin", PlayerPrefs.GetInt("Coin") + 25);
        }
    }
}
