﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Engine : MonoBehaviour {

    public int BoatID;
	// Use this for initialization
	void Start () {

        Eng();
    }

    public void Eng()
    {
        transform.GetChild(0).gameObject.SetActive(false);
        transform.GetChild(1).gameObject.SetActive(false);
        transform.GetChild(2).gameObject.SetActive(false);
        transform.GetChild(3).gameObject.SetActive(false);
        transform.GetChild(4).gameObject.SetActive(false);
        if (PlayerPrefs.GetInt("BoatEngine" + BoatID, 1) == 1)
        {
            transform.GetChild(0).gameObject.SetActive(true);
        }
        if (PlayerPrefs.GetInt("BoatEngine" + BoatID) == 2)
        {
            transform.GetChild(1).gameObject.SetActive(true);
        }
        if (PlayerPrefs.GetInt("BoatEngine" + BoatID) == 3)
        {
            transform.GetChild(2).gameObject.SetActive(true);
        }
        if (PlayerPrefs.GetInt("BoatEngine" + BoatID) == 4)
        {
            transform.GetChild(3).gameObject.SetActive(true);
        }
        if (PlayerPrefs.GetInt("BoatEngine" + BoatID) == 5)
        {
            transform.GetChild(4).gameObject.SetActive(true);
        }
    }
}
