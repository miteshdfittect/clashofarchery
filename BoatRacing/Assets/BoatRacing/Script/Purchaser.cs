﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;

public class Purchaser : MonoBehaviour, IStoreListener
{
    private static IStoreController m_StoreController;          
    private static IExtensionProvider m_StoreExtensionProvider; 

    public string kProductID1 = "consumable";
    public string kProductID2 = "consumable";
    public string kProductID3 = "consumable";
    public string kProductID4 = "consumable";
    public string kProductID5 = "consumable";
    public string kProductIDNonConsumable = "nonconsumable";

    
    void Start()
    {
        if (m_StoreController == null)
        {
            InitializePurchasing();
        }
    }

    public void InitializePurchasing()
    {
        if (IsInitialized())
        {
            return;
        }

        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

        builder.AddProduct(kProductID1, ProductType.Consumable);
        builder.AddProduct(kProductID2, ProductType.Consumable);
        builder.AddProduct(kProductID3, ProductType.Consumable);
        builder.AddProduct(kProductID4, ProductType.Consumable);
        builder.AddProduct(kProductID5, ProductType.Consumable);
        builder.AddProduct(kProductIDNonConsumable, ProductType.NonConsumable);
        UnityPurchasing.Initialize(this, builder);

    }


    private bool IsInitialized()
    {
        return m_StoreController != null && m_StoreExtensionProvider != null;
    }

    public void BuyConsumable1()
    {
        BuyProductID(kProductID1);
    }
    public void BuyConsumable2()
    {
        BuyProductID(kProductID2);
    }
    public void BuyConsumable3()
    {
        BuyProductID(kProductID3);
    }
    public void BuyConsumable4()
    {
        BuyProductID(kProductID4);
    }
    public void BuyConsumable5()
    {
        BuyProductID(kProductID5);
    }
    

    public void BuyNonConsumable()
    {
        BuyProductID(kProductIDNonConsumable);
    }


   

    void BuyProductID(string productId)
    {
        if (IsInitialized())
        {
            Product product = m_StoreController.products.WithID(productId);

            if (product != null && product.availableToPurchase)
            {
                Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                m_StoreController.InitiatePurchase(product);
            }
            else
            {
                Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
            }
        }
        else
        {
            Debug.Log("BuyProductID FAIL. Not initialized.");
        }
    }


    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        Debug.Log("OnInitialized: PASS");

        m_StoreController = controller;
        m_StoreExtensionProvider = extensions;
    }


    public void OnInitializeFailed(InitializationFailureReason error)
    {
        Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
    }


    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
        if (String.Equals(args.purchasedProduct.definition.id, kProductID1, StringComparison.Ordinal))
        {
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
            PlayerPrefs.SetInt("Coin", PlayerPrefs.GetInt("Coin") + 5000);
        }
        else if(String.Equals(args.purchasedProduct.definition.id, kProductID2, StringComparison.Ordinal))
        {
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
            PlayerPrefs.SetInt("Coin", PlayerPrefs.GetInt("Coin") + 12000);
        }
        else if (String.Equals(args.purchasedProduct.definition.id, kProductID3, StringComparison.Ordinal))
        {
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
            PlayerPrefs.SetInt("Coin", PlayerPrefs.GetInt("Coin") + 18000);
        }
        else if (String.Equals(args.purchasedProduct.definition.id, kProductID4, StringComparison.Ordinal))
        {
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
            PlayerPrefs.SetInt("Coin", PlayerPrefs.GetInt("Coin") + 25000);
        }
        else if (String.Equals(args.purchasedProduct.definition.id, kProductID5, StringComparison.Ordinal))
        {
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
            PlayerPrefs.SetInt("Coin", PlayerPrefs.GetInt("Coin") + 50000);
        }
        else if (String.Equals(args.purchasedProduct.definition.id, kProductIDNonConsumable, StringComparison.Ordinal))
        {
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
        }        
        else
        {
            Debug.Log(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));
            PlayerPrefs.SetInt("RemoveAds", 1);
        }

        return PurchaseProcessingResult.Complete;
    }


    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
    }
}
