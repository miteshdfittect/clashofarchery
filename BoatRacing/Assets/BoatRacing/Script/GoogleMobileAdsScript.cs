﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;

// Example script showing how to invoke the Google Mobile Ads Unity plugin.
public class GoogleMobileAdsScript : MonoBehaviour
{

    //public string InterstitialID;
    public string RewardBasedVideoID;

    //private InterstitialAd interstitial;
    private RewardBasedVideoAd rewardBasedVideo;
   
    public void Start()
    {


        // Get singleton reward based video ad reference.
        this.rewardBasedVideo = RewardBasedVideoAd.Instance;

        // RewardBasedVideoAd is a singleton, so handlers should only be registered once.
        this.rewardBasedVideo.OnAdLoaded += this.HandleRewardBasedVideoLoaded;
        this.rewardBasedVideo.OnAdFailedToLoad += this.HandleRewardBasedVideoFailedToLoad;
        this.rewardBasedVideo.OnAdOpening += this.HandleRewardBasedVideoOpened;
        this.rewardBasedVideo.OnAdStarted += this.HandleRewardBasedVideoStarted;
        this.rewardBasedVideo.OnAdRewarded += this.HandleRewardBasedVideoRewarded;
        this.rewardBasedVideo.OnAdClosed += this.HandleRewardBasedVideoClosed;
        this.rewardBasedVideo.OnAdLeavingApplication += this.HandleRewardBasedVideoLeftApplication;

        //RequestInterstitial();
        RequestRewardBasedVideo();
    }  

    // Returns an ad request with custom ad targeting.
    private AdRequest CreateAdRequest()
    {
        return new AdRequest.Builder()
            .AddKeyword("game")            
            .Build();
    }
       
    //private void RequestInterstitial()
    //{


    //    // Clean up interstitial ad before creating a new one.
    //    if (this.interstitial != null)
    //    {
    //        this.interstitial.Destroy();
    //    }

    //    // Create an interstitial.
    //    this.interstitial = new InterstitialAd(InterstitialID);
        
    //    // Load an interstitial ad.
    //    this.interstitial.LoadAd(this.CreateAdRequest());
    //}

    private void RequestRewardBasedVideo()
    {
        this.rewardBasedVideo.LoadAd(this.CreateAdRequest(), RewardBasedVideoID);
    }

    //public void ShowInterstitial()
    //{
    //    if (this.interstitial.IsLoaded())
    //    {
    //        this.interstitial.Show();
    //    }
    //    else
    //    {
    //        MonoBehaviour.print("Interstitial is not ready yet");
    //    }
    //    RequestInterstitial();
    //}


    IEnumerator checkInternetConnection()
    {
        WWW www = new WWW("http://google.com");
        yield return www;
        if (www.error != null)
        {
            CanvasManager.Instent.LoaderPanel.SetActive(false);
            MessagePanel.Instent.Show("You have no Internet Connection\n Check your Internet Connection!!!", "Okay");
        }
        else
        {
            if (this.rewardBasedVideo.IsLoaded())
            {
                CanvasManager.Instent.LoaderPanel.SetActive(false);
                this.rewardBasedVideo.Show();
            }
            else
            {
                StartCoroutine(VideoRecst());
            }
        }
    }
    int VideoCount = 0;
    IEnumerator VideoRecst()
    {
        this.rewardBasedVideo.LoadAd(this.CreateAdRequest(), RewardBasedVideoID);
        yield return rewardBasedVideo.IsLoaded();
        if (this.rewardBasedVideo.IsLoaded())
        {
            CanvasManager.Instent.LoaderPanel.SetActive(false);
            this.rewardBasedVideo.Show();
            VideoCount = 0;
        }
        else if (VideoCount < 15)
        {
            VideoCount++;
            StartCoroutine(VideoRecst());
        }
        else
        {
            VideoCount = 0;
            CanvasManager.Instent.LoaderPanel.SetActive(false);
            MessagePanel.Instent.Show("Reward video ad is not ready yet", "Retry", "Close", () =>
            {
                StartCoroutine(checkInternetConnection());
            });
        }

    }

    public void ShowRewardBasedVideo()
    {
        StartCoroutine(checkInternetConnection()); ;
    }

    #region RewardBasedVideo callback handlers

    public void HandleRewardBasedVideoLoaded(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoLoaded event received");
    }

    public void HandleRewardBasedVideoFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        MonoBehaviour.print(
            "HandleRewardBasedVideoFailedToLoad event received with message: " + args.Message);
    }

    public void HandleRewardBasedVideoOpened(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoOpened event received");
    }

    public void HandleRewardBasedVideoStarted(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoStarted event received");
    }

    public void HandleRewardBasedVideoClosed(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoClosed event received");
        RequestRewardBasedVideo();
    }

    public void HandleRewardBasedVideoRewarded(object sender, Reward args)
    {
        //string type = args.Type;
        //double amount = args.Amount;        
        MessagePanel.Instent.Show("Congratulations !!! you get 500 Reward", "Collect Reward",()=> {
            PlayerPrefs.SetInt("Coin", PlayerPrefs.GetInt("Coin") + 500);
        });

        //MonoBehaviour.print("HandleRewardBasedVideoRewarded event received for " + amount.ToString() + " " + type);
    }

    public void HandleRewardBasedVideoLeftApplication(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoLeftApplication event received");
    }

    #endregion
}