﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoatSelection : MonoBehaviour {

    public List<GameObject> Boat;
    // Use this for initialization
    GameObject SelectedBoat;
    private void Awake()
    {
        SelectedBoat = Instantiate(Boat[PlayerPrefs.GetInt("SelectedBoat", 0)], gameObject.transform.position, transform.rotation);
        SelectedBoat.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
