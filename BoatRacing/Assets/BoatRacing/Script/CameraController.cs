﻿using UnityEngine;
using System.Collections;
#region Delegates
#endregion
public class CameraController : MonoBehaviour {

	#region Private References      
    [SerializeField, Range(0.0f, 1.0f)]
    private float _lerpRate;
    private float _xRotation;
    private float _yYRotation;

	#endregion

	#region Private Methods
    private void Rotate(float xMovement, float yMovement)
    {
        _xRotation += xMovement;
        _yYRotation += yMovement;
    }
	#endregion

	#region Unity CallBacks
	
	void Start ()
	{
        InputManager.MouseMoved += Rotate;
	}

	void Update ()
	{
        _xRotation = Mathf.Lerp(_xRotation, 0, _lerpRate);
        _yYRotation = Mathf.Lerp(_yYRotation, 0, _lerpRate);

        //if (transform.eulerAngles.z > 65 && transform.eulerAngles.z < 100)
        //{
        //    //transform.eulerAngles += new Vector3(0, _xRotation, -_yYRotation-9);
        //    //transform.eulerAngles = Vector3.Lerp(transform.eulerAngles, new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.y - 10), Time.deltaTime * 10);
        //    transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, 62);
        //    //_yYRotation = _yYRotation * -1;

        //}
        //else if (transform.eulerAngles.z > 100 && transform.eulerAngles.z < 360)
        //{
        //    //_yYRotation = _yYRotation * -1;
        //    //transform.eulerAngles += new Vector3(0, _xRotation, -_yYRotation+10);
        //    //transform.eulerAngles = Vector3.Lerp(transform.eulerAngles,new Vector3( transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.y + 10),Time.deltaTime*10);
        //    transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, 350);
        //}


        //print("transform.eulerAngles->" + WrapAngle(transform.localEulerAngles.z));
        //if (WrapAngle(transform.localEulerAngles.z) >= -10 && WrapAngle(transform.localEulerAngles.z) < 65)
        //{
        //    transform.eulerAngles += new Vector3(0, _xRotation, -_yYRotation);
        //}
        //else
        //{
            if (WrapAngle(transform.localEulerAngles.z) < -10 && _yYRotation != 0)
            {
            _yYRotation = 0;
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, -10);

            }
            else if (WrapAngle(transform.localEulerAngles.z) > 65 && _yYRotation != 0)
            {
            _yYRotation = 0;
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, 65);
        }
            
                transform.eulerAngles += new Vector3(0, _xRotation, -_yYRotation);
            
        //}
        
        

	}
    private float WrapAngle(float angle)
    {
        angle %= 360;
        if (angle > 180)
            return angle - 360;

        return angle;
    }

    void OnDestroy()
   {
       InputManager.MouseMoved += Rotate;    
   }
	#endregion
}
