﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Accessories : MonoBehaviour {

    public int BoatID;
	// Use this for initialization
	void Start () {

        Access();
    }
    public void Access()
    {
        if (PlayerPrefs.GetInt("BoatAccessories" + BoatID + "1", 0) == 1)
        {
            transform.GetChild(0).gameObject.SetActive(true);
        }
        if (PlayerPrefs.GetInt("BoatAccessories" + BoatID + "2", 0) == 1)
        {
            transform.GetChild(1).gameObject.SetActive(true);
        }
        if (PlayerPrefs.GetInt("BoatAccessories" + BoatID + "3", 0) == 1)
        {
            transform.GetChild(2).gameObject.SetActive(true);
        }
    }
	
}
