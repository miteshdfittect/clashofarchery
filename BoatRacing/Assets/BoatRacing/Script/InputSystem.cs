﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputSystem : MonoBehaviour {

    public static InputSystem Instent;
    // Use this for initialization
    float VerticalInput;
    float HorizontalInput;
    BoatController boatC;
    void Start () {
        Instent = this;
        boatC = GameObject.FindObjectOfType<BoatController>();
    }

    // Update is called once per frame
    void Update()
    {
//#if UNITY_EDITOR
//         EditorInput();
//#endif
        boatC.setInputs(VerticalInput , HorizontalInput);
    }
    void EditorInput()
    {
        VerticalInput = Input.GetAxisRaw("Vertical");
        HorizontalInput = Input.GetAxisRaw("Horizontal");
    }
    public void MobileInputVertical(float Input)
    {
        VerticalInput = Input;
    }
    public void MobileInputHorizontal(float Input)
    {
        HorizontalInput = Input;
    }
}
