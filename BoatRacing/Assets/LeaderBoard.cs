﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GooglePlayGames;
using System;
using GooglePlayGames.BasicApi;

public class LeaderBoard : MonoBehaviour
{
    public string leaderboard;
    public static LeaderBoard Instent { get; set; }
    public static bool FastTimeOpen = false;
    void Start()
    {
        if (Instent == null)
        {
            Instent = this;
            DontDestroyOnLoad(this.gameObject);            
        }
        else
        {
            Destroy(this.gameObject);
        }


        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
        .Build();

        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.Activate();
        // END THE CODE TO PASTE INTO START
        PlayGamesPlatform.Instance.Authenticate(SignInCallback, true);
        LogIn();
    }

    public void LogIn()
    {
        Social.localUser.Authenticate((bool success) =>
        {
            if (success)
            {
                Debug.Log("Login Sucess");
            }
            else
            {
                Debug.Log("Login failed");
            }
        });
    }

    public void SignInCallback(bool success)
    {
        if (success)
        {
            Debug.Log("(Lollygagger) Signed in!");
        }
        else
        {
            Debug.Log("(Lollygagger) Sign-in failed...");
        }
    }

    public void OnShowLeaderBoard()
    {
        Debug.Log("showLeaderboard Sucess");
        if (PlayGamesPlatform.Instance.localUser.authenticated)
        {
            PlayGamesPlatform.Instance.ShowLeaderboardUI();
        }
        else
        {
            Debug.Log("Cannot show leaderboard: not authenticated");
        }
    }

    public void OnAddScoreToLeaderBorad(int Scor)
    {
        if (Social.localUser.authenticated)
        {
            Social.ReportScore(Scor, leaderboard, (bool success) =>
            {
                if (success)
                {
                    Debug.Log("Update Score Success");

                }
                else
                {
                    Debug.Log("Update Score Fail");
                }
            });
        }
    }

    public void OnLogOut()
    {
        ((PlayGamesPlatform)Social.Active).SignOut();
    }
    
    public void Show()
	{
       // AdsManager.Instent.ShowInterstitial();
        CanvasManager.Instent.LoaderPanel.SetActive(true);
		StartCoroutine(checkInternetConnection());
	}

	IEnumerator checkInternetConnection()
    {
        WWW www = new WWW("http://google.com");
        yield return www;
        if (www.error != null)
        {

            CanvasManager.Instent.LoaderPanel.SetActive(false);
            MessagePanel.Instent.Show("You have no Internet Connection\n Check your Internet Connection!!!", "Okay");
        }
        else
        {
            LogIn();
            CanvasManager.Instent.LoaderPanel.SetActive(false);
            OnShowLeaderBoard();
                       
        }
    }
}