��                      	   SHORE_OFF      FOGOFF     DCOFF     #ifdef VERTEX
#version 300 es

uniform 	vec4 _SinTime;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 _ProjectionParams;
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	mediump float _Size;
uniform 	mediump float _FoamFactor;
uniform 	mediump vec4 _SunDir;
uniform 	mediump float _Translucency;
in highp vec4 in_POSITION0;
in highp vec4 in_TANGENT0;
in highp vec3 in_NORMAL0;
out mediump vec4 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out mediump vec4 vs_TEXCOORD3;
out mediump vec3 vs_TEXCOORD4;
out highp vec2 vs_TEXCOORD5;
out mediump vec3 vs_TEXCOORD6;
vec4 u_xlat0;
vec4 u_xlat1;
vec4 u_xlat2;
mediump vec3 u_xlat16_3;
mediump vec3 u_xlat16_4;
float u_xlat15;
mediump float u_xlat16_18;
void main()
{
    u_xlat0 = in_POSITION0.xxxx * hlslcc_mtx4x4unity_ObjectToWorld[0];
    u_xlat1 = hlslcc_mtx4x4unity_ObjectToWorld[1] * in_POSITION0.yyyy + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat1;
    u_xlat1 = u_xlat1 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat2 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat2 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat2;
    u_xlat2 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat2;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat2;
    u_xlat1.xyz = u_xlat0.yyy * hlslcc_mtx4x4unity_MatrixVP[1].xyw;
    u_xlat1.xyz = hlslcc_mtx4x4unity_MatrixVP[0].xyw * u_xlat0.xxx + u_xlat1.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_MatrixVP[2].xyw * u_xlat0.zzz + u_xlat1.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_MatrixVP[3].xyw * u_xlat0.www + u_xlat0.xyz;
    u_xlat16_3.xy = u_xlat0.yx * vec2(0.5, 0.5);
    u_xlat16_3.z = u_xlat16_3.x * _ProjectionParams.x;
    u_xlat16_3.xy = vec2(u_xlat16_3.y / u_xlat0.z, u_xlat16_3.z / u_xlat0.z);
    vs_TEXCOORD0.xy = u_xlat16_3.xy + vec2(0.5, 0.5);
    vs_TEXCOORD0.zw = vec2(0.0, 0.0);
    vs_TEXCOORD1.z = in_TANGENT0.w * _FoamFactor;
    u_xlat0.xy = in_POSITION0.xz * vec2(_Size);
    vs_TEXCOORD1.xy = u_xlat0.xy;
    vs_TEXCOORD5.xy = _SinTime.xx * vec2(0.300000012, 0.300000012) + u_xlat0.xy;
    u_xlat0.x = dot(in_NORMAL0.xyz, in_NORMAL0.xyz);
    u_xlat0.x = inversesqrt(u_xlat0.x);
    u_xlat0.xyz = u_xlat0.xxx * in_NORMAL0.zxy;
    u_xlat15 = dot(in_TANGENT0.xyz, in_TANGENT0.xyz);
    u_xlat15 = inversesqrt(u_xlat15);
    u_xlat1.xyz = vec3(u_xlat15) * in_TANGENT0.yzx;
    u_xlat2.xyz = u_xlat0.xyz * u_xlat1.xyz;
    u_xlat0.xyz = u_xlat0.zxy * u_xlat1.yzx + (-u_xlat2.xyz);
    u_xlat1.xyz = _WorldSpaceCameraPos.yyy * hlslcc_mtx4x4unity_WorldToObject[1].xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_WorldToObject[0].xyz * _WorldSpaceCameraPos.xxx + u_xlat1.xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_WorldToObject[2].xyz * _WorldSpaceCameraPos.zzz + u_xlat1.xyz;
    u_xlat1.xyz = u_xlat1.xyz + hlslcc_mtx4x4unity_WorldToObject[3].xyz;
    u_xlat1.xyz = u_xlat1.xyz + (-in_POSITION0.xyz);
    u_xlat16_3.y = dot(u_xlat0.xyz, u_xlat1.xyz);
    u_xlat16_4.y = dot(u_xlat0.xyz, _SunDir.xyz);
    u_xlat16_3.x = dot(in_TANGENT0.xyz, u_xlat1.xyz);
    u_xlat16_3.z = dot(in_NORMAL0.xyz, u_xlat1.xyz);
    u_xlat16_18 = dot(u_xlat16_3.xyz, u_xlat16_3.xyz);
    u_xlat16_18 = inversesqrt(u_xlat16_18);
    u_xlat16_3.xyz = vec3(u_xlat16_18) * u_xlat16_3.xyz;
    u_xlat16_4.x = dot(in_TANGENT0.xyz, _SunDir.xyz);
    u_xlat16_4.z = dot(in_NORMAL0.xyz, _SunDir.xyz);
    u_xlat0.xyz = (-u_xlat16_4.xyz) + in_NORMAL0.xyz;
    vs_TEXCOORD4.xyz = u_xlat16_4.xyz;
    u_xlat16_18 = dot(u_xlat16_3.xyz, (-u_xlat0.xyz));
    vs_TEXCOORD6.xyz = u_xlat16_3.xyz;
    u_xlat16_3.x = max(u_xlat16_18, 0.0);
    u_xlat16_3.x = u_xlat16_3.x * _Translucency;
    vs_TEXCOORD3.w = u_xlat16_3.x * 0.5;
    vs_TEXCOORD3.xyz = in_NORMAL0.xyz;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	mediump float _FoamSize;
uniform 	mediump vec4 _SurfaceColor;
uniform 	mediump vec4 _WaterColor;
uniform 	mediump float _Specularity;
uniform 	mediump float _SpecPower;
uniform 	mediump vec4 _SunColor;
uniform lowp sampler2D _Foam;
uniform lowp sampler2D _Bump;
uniform lowp sampler2D _Reflection;
uniform lowp sampler2D _Refraction;
in mediump vec4 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in mediump vec4 vs_TEXCOORD3;
in mediump vec3 vs_TEXCOORD4;
in highp vec2 vs_TEXCOORD5;
in mediump vec3 vs_TEXCOORD6;
layout(location = 0) out mediump vec4 SV_Target0;
vec2 u_xlat0;
mediump vec3 u_xlat16_0;
lowp vec3 u_xlat10_0;
mediump vec3 u_xlat16_1;
mediump vec3 u_xlat16_2;
mediump vec3 u_xlat16_3;
lowp vec3 u_xlat10_3;
mediump vec3 u_xlat16_4;
mediump vec3 u_xlat16_6;
mediump vec2 u_xlat16_11;
mediump float u_xlat16_16;
void main()
{
    u_xlat0.xy = (-vs_TEXCOORD5.xy) * vec2(vec2(_FoamSize, _FoamSize));
    u_xlat10_0.x = texture(_Foam, u_xlat0.xy).x;
    u_xlat16_1.x = u_xlat10_0.x + -0.5;
#ifdef UNITY_ADRENO_ES3
    u_xlat16_1.x = min(max(u_xlat16_1.x, 0.0), 1.0);
#else
    u_xlat16_1.x = clamp(u_xlat16_1.x, 0.0, 1.0);
#endif
    u_xlat16_1.x = u_xlat16_1.x * vs_TEXCOORD1.z;
#ifdef UNITY_ADRENO_ES3
    u_xlat16_1.x = min(max(u_xlat16_1.x, 0.0), 1.0);
#else
    u_xlat16_1.x = clamp(u_xlat16_1.x, 0.0, 1.0);
#endif
    u_xlat16_1.x = u_xlat16_1.x * _SunColor.z;
    u_xlat10_0.xyz = texture(_Bump, vs_TEXCOORD5.xy).xyz;
    u_xlat16_0.xyz = u_xlat10_0.xyz * vec3(2.4000001, 2.4000001, 2.4000001) + vec3(-1.0, -1.0, -1.0);
    u_xlat16_6.x = dot(u_xlat16_0.xyz, u_xlat16_0.xyz);
    u_xlat16_6.x = inversesqrt(u_xlat16_6.x);
    u_xlat16_11.xy = u_xlat16_0.xy * u_xlat16_6.xx + vs_TEXCOORD3.xz;
    u_xlat16_2.xyz = u_xlat16_0.xyz * u_xlat16_6.xxx;
    u_xlat16_6.x = dot(vs_TEXCOORD6.xyz, u_xlat16_0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat16_6.x = min(max(u_xlat16_6.x, 0.0), 1.0);
#else
    u_xlat16_6.x = clamp(u_xlat16_6.x, 0.0, 1.0);
#endif
    u_xlat16_6.x = (-u_xlat16_6.x) + 1.0;
    u_xlat16_11.xy = u_xlat16_11.xy * vec2(0.0500000007, 0.0500000007) + vs_TEXCOORD0.xy;
    u_xlat10_0.xyz = texture(_Reflection, u_xlat16_11.xy).xyz;
    u_xlat10_3.xyz = texture(_Refraction, u_xlat16_11.xy).xyz;
    u_xlat16_3.xyz = u_xlat10_3.xyz * _WaterColor.xyz;
    u_xlat16_4.xyz = u_xlat10_0.xyz * _SurfaceColor.xyz + (-u_xlat16_3.xyz);
    u_xlat16_6.xyz = u_xlat16_6.xxx * u_xlat16_4.xyz + u_xlat16_3.xyz;
    u_xlat16_1.xyz = u_xlat16_6.xyz * _SunColor.xyz + u_xlat16_1.xxx;
    u_xlat16_16 = dot(vs_TEXCOORD4.xyz, vs_TEXCOORD4.xyz);
    u_xlat16_16 = inversesqrt(u_xlat16_16);
    u_xlat16_4.xyz = (-vs_TEXCOORD4.xyz) * vec3(u_xlat16_16) + vs_TEXCOORD6.xyz;
    u_xlat16_16 = dot(u_xlat16_4.xyz, u_xlat16_4.xyz);
    u_xlat16_16 = inversesqrt(u_xlat16_16);
    u_xlat16_4.xyz = vec3(u_xlat16_16) * u_xlat16_4.xyz;
    u_xlat16_16 = dot(u_xlat16_4.xyz, u_xlat16_2.xyz);
    u_xlat16_16 = max(u_xlat16_16, 0.0);
    u_xlat16_16 = log2(u_xlat16_16);
    u_xlat16_2.x = _Specularity * 250.0;
    u_xlat16_16 = u_xlat16_16 * u_xlat16_2.x;
    u_xlat16_16 = exp2(u_xlat16_16);
    u_xlat16_16 = u_xlat16_16 * _SpecPower;
    u_xlat16_1.xyz = vec3(u_xlat16_16) * _SunColor.xyz + u_xlat16_1.xyz;
    u_xlat16_2.xyz = vs_TEXCOORD3.www * _WaterColor.xyz;
    SV_Target0.xyz = u_xlat16_2.xyz * _SunColor.xyz + u_xlat16_1.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif
  �                              